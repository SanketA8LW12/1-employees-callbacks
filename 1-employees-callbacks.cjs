/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const { error } = require('console');
const fs = require('fs');
const path = require('path');

let dataRetrive = fs.readFile('./data.json', 'utf-8', (error, data) => {
    if (error) {
        console.log(error);
    }
    else {

        let newData = JSON.parse(data).employees;

        const retriveData = newData.filter((data) => {
            return data.id == 2 || data.id == 13 || data.id == 23;
        })

        fs.writeFile('./probelm1Output.json', JSON.stringify(retriveData), (err) => {
            if (err) {
                console.error(err);
            }
        });
        const group = newData.reduce((acc, curr) => {

            if (acc[curr.company]) {
                let obj2 = {};
                obj2.id = curr.id;
                obj2.name = curr.name;
                acc[curr.company].push(obj2)
            } else {
                let arr = [];
                let obj = {};
                obj.id = curr.id;
                obj.name = curr.name;
                arr.push(obj);
                acc[curr.company] = arr;
            }

            return acc;

        }, {});

        fs.writeFile('./problem2Output.json', JSON.stringify(group), (err) => {
            if (err) {
                console.error(err);
            } else {
                const Powerpuff_Brigade = newData.filter((data) => {
                    return data.company === "Powerpuff Brigade"
                })

                fs.writeFile('./problem3Output.json', JSON.stringify(Powerpuff_Brigade), (err) => {
                    if (err) {
                        console.error(err);
                    }
                });
            }
        })


    }
})

